﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123.Models;

namespace stock123.Controllers
{
    public class Tipo_InventarioController : Controller
    {
        private dbModels db = new dbModels();

        // GET: Tipo_Inventario
        public ActionResult Index()
        {
            var tipo_inventario = db.tipo_inventario.Include(t => t.almacen);
            return View(tipo_inventario.ToList());
        }

        // GET: Tipo_Inventario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            return View(tipo_inventario);
        }

        // GET: Tipo_Inventario/Create
        public ActionResult Create()
        {
            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion");
            return View();
        }

        // POST: Tipo_Inventario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_almacen,descripcion,cuenta_contable,estado")] tipo_inventario tipo_inventario)
        {
            if (ModelState.IsValid)
            {
                db.tipo_inventario.Add(tipo_inventario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion", tipo_inventario.id_almacen);
            return View(tipo_inventario);
        }

        // GET: Tipo_Inventario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion", tipo_inventario.id_almacen);
            return View(tipo_inventario);
        }

        // POST: Tipo_Inventario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_almacen,descripcion,cuenta_contable,estado")] tipo_inventario tipo_inventario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_inventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion", tipo_inventario.id_almacen);
            return View(tipo_inventario);
        }

        // GET: Tipo_Inventario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            return View(tipo_inventario);
        }

        // POST: Tipo_Inventario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            db.tipo_inventario.Remove(tipo_inventario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
