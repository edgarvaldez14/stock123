﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123.Models;

namespace stock123.Controllers
{
    public class articulosController : Controller
    {
        private dbModels db = new dbModels();

        // GET: articulos
        public ActionResult Index()
        {
            var articuloes = db.articuloes.Include(a => a.tipo_inventario1);
            return View(articuloes.ToList());
        }

        // GET: articulos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }
            return View(articulo);
        }

        // GET: articulos/Create
        public ActionResult Create()
        {
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion");
            return View();
        }

        // POST: articulos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_articulo,descripcion,existencia,tipo_inventario,costo_unitario,estado")] articulo articulo)
        {
            if (ModelState.IsValid)
            {
                db.articuloes.Add(articulo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", articulo.tipo_inventario);
            return View(articulo);
        }

        // GET: articulos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", articulo.tipo_inventario);
            return View(articulo);
        }

        // POST: articulos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_articulo,descripcion,existencia,tipo_inventario,costo_unitario,estado")] articulo articulo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(articulo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", articulo.tipo_inventario);
            return View(articulo);
        }

        // GET: articulos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }
            return View(articulo);
        }

        // POST: articulos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            articulo articulo = db.articuloes.Find(id);
            db.articuloes.Remove(articulo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
