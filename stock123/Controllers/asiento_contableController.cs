﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123.Models;

namespace stock123.Controllers
{
    public class asiento_contableController : Controller
    {
        private dbModels db = new dbModels();

        // GET: asiento_contable
        public ActionResult Index()
        {
            var asiento_contable = db.asiento_contable.Include(a => a.tipo_inventario1);
            return View(asiento_contable.ToList());
        }

        // GET: asiento_contable/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            return View(asiento_contable);
        }

        // GET: asiento_contable/Create
        public ActionResult Create()
        {
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion");
            return View();
        }

        // POST: asiento_contable/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado")] asiento_contable asiento_contable)
        {
            if (ModelState.IsValid)
            {
                db.asiento_contable.Add(asiento_contable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }

        // GET: asiento_contable/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }

        // POST: asiento_contable/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado")] asiento_contable asiento_contable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asiento_contable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }

        // GET: asiento_contable/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            return View(asiento_contable);
        }

        // POST: asiento_contable/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            db.asiento_contable.Remove(asiento_contable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
