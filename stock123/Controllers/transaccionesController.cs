﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123.Models;

namespace stock123.Controllers
{
    public class transaccionesController : Controller
    {
        private dbModels db = new dbModels();

        // GET: transacciones
        public ActionResult Index()
        {
            var transacciones = db.transacciones.Include(t => t.articulo1);
            return View(transacciones.ToList());
        }

        // GET: transacciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaccione transaccione = db.transacciones.Find(id);
            if (transaccione == null)
            {
                return HttpNotFound();
            }
            return View(transaccione);
        }

        // GET: transacciones/Create
        public ActionResult Create()
        {
            ViewBag.articulo = new SelectList(db.articuloes.Where(t=> t.existencia > 0).ToList(), "id_articulo", "descripcion");
            ViewBag.Error = TempData["Error"];
            return View();
        }

        // POST: transacciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_transaccion,tipo_transaccion,articulo,fecha,cantidad,monto")] transaccione transaccione)
        {
            if (ModelState.IsValid)
            {
                db.transacciones.Add(transaccione);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Error = TempData["Error"];
            ViewBag.articulo = new SelectList(db.articuloes, "id_articulo", "descripcion", transaccione.articulo);
            return View(transaccione);
        }

        // GET: transacciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaccione transaccione = db.transacciones.Find(id);
            if (transaccione == null)
            {
                return HttpNotFound();
            }
            ViewBag.articulo = new SelectList(db.articuloes, "id_articulo", "descripcion", transaccione.articulo);
            return View(transaccione);
        }

        // POST: transacciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_transaccion,tipo_transaccion,articulo,fecha,cantidad,monto")] transaccione transaccione)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaccione).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.articulo = new SelectList(db.articuloes, "id_articulo", "descripcion", transaccione.articulo);
            return View(transaccione);
        }

        // GET: transacciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaccione transaccione = db.transacciones.Find(id);
            if (transaccione == null)
            {
                return HttpNotFound();
            }
            return View(transaccione);
        }

        // POST: transacciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            transaccione transaccione = db.transacciones.Find(id);
            db.transacciones.Remove(transaccione);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost()]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTransaction(transaccione Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var articulos = db.articuloes;
                    var articuloSelected = articulos.FirstOrDefault(t => t.id_articulo == Model.articulo.Value);
                    if(Model.cantidad > articuloSelected.existencia)
                    {
                        TempData["Error"] = "No hay cantidad suficiente de articulos, la cantidad en existencia es " + articuloSelected.existencia;
                        return RedirectToAction("Create");
                    }
                    articuloSelected.existencia -= Model.cantidad;
                    db.Entry(articuloSelected).State = EntityState.Modified;

                    db.transacciones.Add(Model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
