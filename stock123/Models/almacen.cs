namespace stock123.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class almacen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public almacen()
        {
            this.tipo_inventario = new HashSet<tipo_inventario>();
        }
    
        public int id_almacen { get; set; }
        public string descripcion { get; set; }
        public Nullable<bool> estado { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tipo_inventario> tipo_inventario { get; set; }
    }
}
