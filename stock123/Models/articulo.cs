namespace stock123.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class articulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public articulo()
        {
            this.transacciones = new HashSet<transaccione>();
        }
    
        public int id_articulo { get; set; }
        public string descripcion { get; set; }
        [RegularExpression("^[0-9]+$",
            ErrorMessage = "Solo numeros positivos")]
        public int existencia { get; set; }
        public Nullable<int> tipo_inventario { get; set; }
        public float costo_unitario { get; set; }
        public Nullable<bool> estado { get; set; }
    
        public virtual tipo_inventario tipo_inventario1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<transaccione> transacciones { get; set; }
    }
}
