namespace stock123.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class transaccione
    {
        public int id_transaccion { get; set; }
        public string tipo_transaccion { get; set; }
        public Nullable<int> articulo { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public int cantidad { get; set; }
        public float monto { get; set; }
    
        public virtual articulo articulo1 { get; set; }

        [NotMapped]
        public List<articulo> ListArticlulos { get; set; }
    }
}
